/* global TrelloPowerUp */

var Promise = TrelloPowerUp.Promise;

var WHITE_ICON = 'https://TrelloNumberStats.bitbucket.io/res/LogoWhite.svg';
var BLACK_ICON = 'https://TrelloNumberStats.bitbucket.io/res/LogoBlack.svg';

TrelloPowerUp.initialize({
  // Start adding handlers for your capabilities here!
   'board-buttons': function(t, options) {
   	return [{
   		icon: {
        dark: WHITE_ICON,
        light: BLACK_ICON
      },
   		text: 'Number Stats',
      callback: function(t) {
        
        var fields = { };
        var fieldCount = 0;
        
        t.board('customFields').then(function (board) {
          //console.log(JSON.stringify(board, null, 2));
          board.customFields.forEach(function(field) {
            if(field.type == "number") {
              fields[field.id] = { "name": field.name, "sum": 0, "count": 0, "avg": 0 };
              fieldCount++;
              //console.log("Incrementing fc: " + fieldCount);
            }
          });
        });
          
        //console.log(fields);
        
        return t.cards('all')
        .then(function (cards) {
          cards.forEach(function(card) {
            //console.log(card);
            card.customFieldItems.forEach(function(customField) {
              //console.log(customField);
              if(fields[customField.idCustomField] != undefined) {
                fields[customField.idCustomField].count++;
                fields[customField.idCustomField].sum += parseFloat(customField.value.number);
              }
            });
          });
          
          for (var key in fields) {
            if(!fields.hasOwnProperty(key)) continue;
            if(fields[key].count > 0) {
              fields[key].avg = (fields[key].sum / fields[key].count).toFixed(2);
            }
          };
          
          //console.log(fields);
          
          var height = 190;
          
          if(Object.keys(fields).length > 1) {
            height = 360;
          }
          
          if(fieldCount == 0) {
            //console.log(fields);
            //console.log("FC: " + fieldCount);
            t.popup({
              title: "Number Stats",
              url: 'nodata.html',
              height: 340
            });
          }
          else {
            t.popup({
              title: "Number Stats",
              url: 'popup.html',
              args: { myArgs: 'You can access these with t.arg()', fields: fields },
              height: height
            });
          }
        });
      }
   	}];
   },
});
